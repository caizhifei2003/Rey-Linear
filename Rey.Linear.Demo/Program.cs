﻿using System;

namespace Rey.Linear.Demo {
    class Program {
        static void Main(string[] args) {
            //! 1 2
            //! 3 4
            var m2 = new Matrix2F(
                1, 2, 
                3, 4
            );

            //! 1 3
            //! 2 4
            var t2 = m2.Transpose();

            var m3 = new Matrix3F(
                1, 2, 3, 
                4, 5, 6, 
                7, 8, 9
            );

            //! X 1 4 7
            //! Y 2 5 8
            //! Z 3 6 9
            var t3 = m3.Transpose();
        }
    }
}
