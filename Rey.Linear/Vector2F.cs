﻿using System;

namespace Rey.Linear {
    public static class ArrayExtensions {
        public static float[] Swap(float[] arr, int i1, int i2) {
            var temp = arr[i1];
            arr[i1] = arr[i2];
            arr[i2] = temp;
            return arr;
        }

        public static float[,] Swap(float[,] arr, (int, int) i1, (int, int) i2) {
            var temp = arr[i1.Item1, i1.Item2];
            arr[i1.Item1, i1.Item2] = arr[i2.Item1, i2.Item2];
            arr[i2.Item1, i2.Item2] = temp;
            return arr;
        }
    }

    public class Vector2F {
        public float[] Data = { 0f, 0f };
        public float X => this.Data[0];
        public float Y => this.Data[1];

        public Vector2F(float[] data) {
            data.CopyTo(this.Data, 0);
        }

        public Vector2F(float x, float y) {
            this.Data[0] = x;
            this.Data[1] = y;
        }
    }

    public class Vector3F {
        public float[] Data = { 0, 0, 0 };
        public float X => this.Data[0];
        public float Y => this.Data[1];
        public float Z => this.Data[2];

        public Vector3F(float[] data) {
            data.CopyTo(this.Data, 0);
        }

        public Vector3F(float x, float y, float z) {
            this.Data[0] = x;
            this.Data[1] = y;
            this.Data[2] = z;
        }
    }

    public class Matrix2F {
        public float[,] Data = { { 0f, 0f }, { 0f, 0f } };
        public float X1 => this.Data[0, 0];
        public float X2 => this.Data[0, 1];
        public float Y1 => this.Data[1, 0];
        public float Y2 => this.Data[1, 1];

        public Matrix2F(float[,] data) {
            data.CopyTo(this.Data, 0);
        }

        public Matrix2F(float x1, float x2, float y1, float y2) {
            this.Data[0, 0] = x1;
            this.Data[0, 1] = x2;
            this.Data[1, 0] = y1;
            this.Data[1, 1] = y2;
        }

        public Matrix2F Transpose() {
            return new Matrix2F(this.X1, this.Y1, this.X2, this.Y2);
        }
    }

    public class Matrix3F {
        public float[,] Data = { { 0f, 0f, 0f }, { 0f, 0f, 0f }, { 0f, 0f, 0f } };
        public float X1 => this.Data[0, 0];
        public float X2 => this.Data[0, 1];
        public float X3 => this.Data[0, 2];
        public float Y1 => this.Data[1, 0];
        public float Y2 => this.Data[1, 1];
        public float Y3 => this.Data[1, 2];
        public float Z1 => this.Data[2, 0];
        public float Z2 => this.Data[2, 1];
        public float Z3 => this.Data[2, 2];

        public Matrix3F(float[,] data) {
            this.Data = data;
        }

        public Matrix3F(
            float x1, float x2, float x3,
            float y1, float y2, float y3,
            float z1, float z2, float z3
        ) {
            this.Data[0, 0] = x1;
            this.Data[0, 1] = x2;
            this.Data[0, 2] = x3;
            this.Data[1, 0] = y1;
            this.Data[1, 1] = y2;
            this.Data[1, 2] = y3;
            this.Data[2, 0] = z1;
            this.Data[2, 1] = z2;
            this.Data[2, 2] = z3;
        }

        public Matrix3F Transpose() {
            return new Matrix3F(
                this.X1, this.Y1, this.Z1,
                this.X2, this.Y2, this.Z2,
                this.X3, this.Y3, this.Z3
            );
        }
    }
}
